window._CCSettings = {
    platform: "web-mobile",
    groupList: [
        "default",
        "background",
        "orthographic",
        "perspective",
        "after effect",
        "common ui"
    ],
    collisionMatrix: [
        [
            true
        ],
        [
            false,
            false
        ],
        [
            false,
            false,
            false
        ],
        [
            false,
            false,
            false,
            false
        ],
        [
            false,
            false,
            false,
            false,
            false
        ],
        [
            false,
            false,
            false,
            false,
            false,
            false
        ]
    ],
    hasResourcesBundle: true,
    hasStartSceneBundle: false,
    remoteBundles: [],
    subpackages: [],
    launchScene: "db://assets/scenes/home.fire",
    orientation: "portrait",
    debug: true,
    jsList: [],
    bundleVers: {
        internal: "9d52b",
        resources: "9d0cf",
        main: "68549"
    }
};
